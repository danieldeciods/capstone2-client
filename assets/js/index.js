const adminUser = localStorage.getItem("isAdmin")
let navbar = document.querySelector("#zuitterNav")
let header = document.querySelector("#zuitterHeader")

if(localStorage.length > 0){
	let navbar = document.querySelector("#zuitterNav")
	

	if(adminUser == "false" || !adminUser){
		navbar.innerHTML = 
			`
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a href="./index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/courses.html" class="nav-link"> Courses </a>
				</li>

				<li class="nav-item">
					<a href="./pages/profile.html" class="nav-link"> Profile </a>
				</li>

				<li class="nav-item">
					<a href="./pages/logout.html" class="nav-link"> Log Out </a>
				</li>
			</ul>
			`
	} else {

			header.innerHTML = 
				`
				<h1>Zuitter Booking Services</h1>
				<p class="lead"> Booking for everyone, everywhere </p>
				`

			navbar.innerHTML = 
			`
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a href="./index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/courses.html" class="nav-link"> Courses </a>
				</li>

				<li class="nav-item">
					<a href="./pages/logout.html" class="nav-link"> Log Out </a>
				</li>

			</ul>
			`
	}

} else {

	header.innerHTML = 
				`
				<h1>Zuitter Booking Services</h1>
				<p class="lead"> Booking for everyone, everywhere </p>
				<a href="./pages/register.html" class="btn btn-outline-primary" class='m-1'> Register Now! </a>
				<br>
				<a href="./pages/login.html" class="btn btn-outline-primary" class='m-1'> Login </a>
				`


	navbar.innerHTML = 
			`
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a href="./index.html" class="nav-link"> Home </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/courses.html" class="nav-link"> Courses </a>
				</li>

				<li class="nav-item ">
					<a href="./pages/register.html" class="nav-link"> Register </a>
				</li>

				<li class="nav-item">
					<a href="./pages/login.html" class="nav-link"> Log in </a>
				</li>
			</ul>
			`
}
