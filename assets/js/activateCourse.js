let archiveMessage = document.querySelector("#archiveMessage")
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")


let token = localStorage.getItem("token")

	fetch(`https://glacial-anchorage-43228.herokuapp.com/api/courses/${courseId}/activate`, {
		method: "PUT",
		headers: {
			"Content-Type" : "application/json",
			"Authorization" : `Bearer ${token}`
		},
		courseId : params.courseId
	})
  .then(res => {return res.json()})
  .then(data => {
  	archiveMessage.innerHTML = "Course Activated"
  })