
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")


fetch(`https://warm-temple-56667.herokuapp.com/api/courses/${courseId}`, {
	method: "DELETE",
	headers: {
 		"Authorization": `Bearer ${token}`
 	},
}).then(res => {return res.json()
}).then(data => {
 	if(data){
 		alert("You have deleted the course successfully")
 		window.location.replace("./courses.html")
 	} else {
		alert("Deleting failed")
	}
})
	
